let inventory = require("./carData.js");

// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function listCarYearsProblem(inventory)
{
  if (Array.isArray(inventory)) {
    const listCarYears = inventory.map(data => data.car_year);
    return listCarYears;
  } else {
    return null;
  }
}
module.exports = listCarYearsProblem;

