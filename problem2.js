let inventory = require("./carData.js");

// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
//"Last car is a *car make goes here* *car model goes here*"

function findLastCarInInventory(inventory) {
  if (Array.isArray(inventory)) {
    const findLastElementOfInventory = inventory.slice(-1)[0];
    return `Last car is a ${findLastElementOfInventory.car_make} ${findLastElementOfInventory.car_model}`;
  } else {
    return null;
  }
}
module.exports = findLastCarInInventory;
