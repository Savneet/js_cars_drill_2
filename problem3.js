let inventory = require("./carData.js");

// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortCarModels(inventory)
{
  if (Array.isArray(inventory)) {
    const sortCarModelsVar = inventory.sort((carData1, carData2) => {
      carData1.car_model = carData1.car_model.toLowerCase();
      carData2.car_model = carData2.car_model.toLowerCase();
      if (carData1.car_model < carData2.car_model) {
        return -1;
      }
  
      if (carData1.car_model > carData2.car_model) {
        return 1;
      }
      return 0;
    });
    return sortCarModelsVar;
  } else {
    return null;
  }
}
module.exports = sortCarModels;

