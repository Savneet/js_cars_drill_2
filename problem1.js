let inventory = require("./carData.js");

// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

function findCarWithId(inventory, id)
{
  if (Array.isArray(inventory) && typeof id === "number") {
    const findCarWithId = inventory.find(function (carData) {
      if (carData.id === 33) {
        return true;
      } else {
        return false;
      }
    });
    if(findCarWithId)
    {
      return `Car 33 is a ${findCarWithId.car_year} ${findCarWithId.car_make} ${findCarWithId.car_model}`;
    }
    else
    {
      return `No Car with Id 33`;
    }
  } else {
    return null;
  }
}

module.exports = findCarWithId;