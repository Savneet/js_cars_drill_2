let inventory = require("./carData.js");

// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function listBmwAndAudiCarsProblem(inventory)
{
  if (Array.isArray(inventory)) {
    const listBmwAndAudiCars = inventory.filter(function (data) {
      if (data.car_make === "BMW" || data.car_make === "Audi") {
        return true;
      }
    });
    return listBmwAndAudiCars;
  } else {
    return null;
  }
}
module.exports = listBmwAndAudiCarsProblem;
